package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

// Tests the Quoter
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price1 = quoter.getBookPrice("1");
        double price2 = quoter.getBookPrice("2");
        double price3 = quoter.getBookPrice("3");
        double price4 = quoter.getBookPrice("4");
        double price5 = quoter.getBookPrice("5");
        Assertions.assertEquals(10.0, price1, 0.0, "Price of book 1");
        Assertions.assertEquals(45.0, price2, 0.0, "Price of book 2");
        Assertions.assertEquals(20.0, price3, 0.0, "Price of book 3");
        Assertions.assertEquals(35.0, price4, 0.0, "Price of book 4");
        Assertions.assertEquals(50.0, price5, 0.0, "Price of book 5");
    }
}
