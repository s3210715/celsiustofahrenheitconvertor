package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class CtFT {

    public double getFahrenheitTemperature(double celsius) {
        return celsius * 1.8 + 32;
    }
}
